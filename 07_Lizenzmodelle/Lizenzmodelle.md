# Lizenzmodelle

1. **Erklären Sie das Wort Lizenz. Was bedeutet das?**

   Die Schaffung von Lizenzen dient mehreren Zwecken und geht oft auf die Notwendigkeit zurück, die Rechte, Nutzung und Verbreitung von bestimmten Ressourcen oder Produkten zu regeln. Einerseits kann es für Schutz eines geistigen Eigentums wie Software, Musik oder Kunstwerke bieten, aber auch für die Kontrolle über die Nutzung helfen. Lizenzen bieten dem Inhaber die Möglichkeit, die Art und Weise zu kontrollieren, wie sein Eigentum verwendet wird.

3. **Worauf gibt es alles Lizenzen ausser für Software?**

   Urheberrechtslizenzen

   - Lizenzen für Bücher, Musik, Filme, Kunstwerke und andere kreative Werke. Diese Lizenzen regeln die Nutzung, Vervielfältigung und Verbreitung geschützter Inhalte.

   Patentlizenzen

   - Lizenzen für patentierte Erfindungen. Patentinhaber können Lizenzen vergeben, um anderen die Nutzung, Herstellung oder den Verkauf ihrer patentierten Technologien zu erlauben.

   Markenlizenzen

   - Lizenzen für Marken und Logos. Unternehmen können anderen die Nutzung ihrer Marken gegen Gebühren gestatten, um Produkte oder Dienstleistungen unter einer etablierten Marke zu vermarkten.

   Geschäftslizenzen

   - Lizenzen, die für den Betrieb eines Geschäfts oder Unternehmens erforderlich sind. Dazu gehören Baugenehmigungen, Umweltlizenzen und andere behördliche Genehmigungen.

   Immobilienlizenzen

   - Lizenzen im Immobilienbereich, die es Personen erlauben, Immobilienmakler, -verwalter oder -entwickler zu werden.

4. **Machen Sie eine Aufzählung von Lizenzmodellen**

   Software-Lizenzmodelle:

   - Proprietäre Software

     - Ein proprietäres Software-Lizenzmodell basiert darauf, dass Softwareunternehmen die Software erstellen und die Kontrolle über ihren Code und damit über ihre Funktionen und ihre Nutzung behalten, was in erster Linie der langfristigen Monetarisierung der Software durch den Entwickler dient.

   - Freie und quelloffene Software (FOSS = Free and Open Source Software) oder auch "Open Source"

5. *Was bedeutet "open source" und was kann man mit solcher Software machen?*

- Offene Lizenzmodelle für Software gestatten dem Nutzer die Befugnis, Anpassungen am Quellcode der Software vorzunehmen, der gemeinsam mit dem Softwareprodukt bereitgestellt wird.
In beiden Szenarien sind in der Regel in der Softwarelizenz Bestimmungen enthalten, die die Haftung bei der Nutzung des Softwareprodukts einschränken. Es werden auch gegenseitige Verpflichtungen, wie beispielsweise Support, sowie Garantien oder Ausschlüsse von Garantien festgelegt.

6. *Was ist der Unterschied zwischen "copyright" und "copyleft"*

- Copyright:

"Copyright" bezieht sich auf das gesetzliche Recht, das dem Urheber eines kreativen Werks gewährt wird. Dieses Recht ermöglicht es dem Urheber, zu kontrollieren, wie sein Werk reproduziert, verteilt, performt und modifiziert wird. Das Copyright wird automatisch gewährt, wenn das Werk geschaffen wird, und es ist nicht erforderlich, es zu registrieren. Die Dauer des Urheberrechts variiert je nach Land, liegt jedoch oft bei mehreren Jahrzehnten.

- Copyleft:

"Copyleft" ist eine spezifische Strategie im Umgang mit Urheberrechten, die darauf abzielt, die Freiheit und Offenheit von Software zu fördern. Im Kontext von Software bedeutet Copyleft, dass der Urheber die Verwendung, Modifikation und Verbreitung der Software erlaubt, aber unter der Bedingung, dass abgeleitete Werke ebenfalls unter derselben Lizenz veröffentlicht werden müssen. Mit anderen Worten, Copyleft setzt die Freiheit, Software zu nutzen und zu modifizieren, voraus, aber es schützt auch die Freiheit für zukünftige Versionen.

7. *Welches Lizenz-Modell wird angewendet, wenn man...*

im App-Store eine zu bezahlende App herunterlädt und installiert?

- Einmaliger Kauf (Pay-to-Own): Dieses Lizenzmodell erlaubt es dem Benutzer die App nach der Zahlung dauerhaft zu behalten und zu benutzen, ohne zusätzliche Gebühren.

- Abonnement: Der Benutzer zahlt bei diesem Modell regelmässig, normalerweise monatlich oder jährlich, um Zugriff auf die App oder auf Funktionen innerhalb der App zuzugreifen. Solange das Abonnement gültig ist, kann der Benutzer auf die App und/oder deren Funktionen zugreifen

- In-App-Käufe: Bei kostenlos-herunterladbare Apps gibt es oft die Möglichkeit für In-App-Käufe. Das heisst, dass Benutzer innerhalb der App weitere Inhalte und Funktionen kaufen können.


im App-Store eine Gratis-App heunterlädt und installiert?

- Freemium: Bei diesem Modell können Benutzer die App kostenlos herunterladen und grundlegende Funktionen nutzen. Es ist grundsätzlich das Prinzip von In-App-Käufen.

- Werbefinanzierte Apps: Diese Apps finanzieren sich durch Werbung. Für den Benutzer mag die App gratis sein, jedoch werden innerhalb der App Anzeigen angezeigt.

- Spenden/Unterstützungsmodell: Manchmal werden Apps auch durch freiwilligen Spenden der Benutzer finanziert.


