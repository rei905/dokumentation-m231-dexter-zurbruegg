 1. **Datensicherungsziele, Datenkompression**

 **Welche Medien/Datenträger/Geräte werden als Backup-Ziel verwenden?**

- **Externe Festplatten:**
 Externe Festplatten sind beliebte Backup-Ziele aufgrund ihrer großen Kapazität und Mobilität. Sie ermöglichen schnelle und einfache Sicherungen von Daten, die dann offline aufbewahrt werden können.

- **USB-Sticks:**
 USB-Sticks bieten eine kompakte und tragbare Lösung für kleinere Datensicherungen. Sie eignen sich gut für den Transfer kleinerer Datenmengen und sind einfach zu handhaben.

- **Network Attached Storages (NAS):** NAS-Geräte sind spezielle Geräte, die mit einem Netzwerk verbunden sind und als zentraler Speicherort für Backups dienen. Sie ermöglichen den einfachen Zugriff auf gesicherte Daten von verschiedenen Geräten im Netzwerk.

- **SSDs:**
 Solid State Drives bieten schnelle Lese- und Schreibgeschwindigkeiten, was Backup-Prozesse beschleunigen kann. Sie sind eine effiziente Option für schnelle und zuverlässige Datensicherungen.

- **Interne Speicherplatten (im Computer eingebaut):**
 Interne Festplatten oder SSDs, die direkt im Computer eingebaut sind, können ebenfalls als Backup-Ziele verwendet werden. Dies ist besonders sinnvoll, wenn Platz für zusätzliche interne Speicher vorhanden ist.

 **Was für Kompressionsverfahren werden verwendet?**

- **ZIP:**
 Das ZIP-Kompressionsformat ist weit verbreitet und wird von vielen Betriebssystemen unterstützt. Es ermöglicht die Bündelung mehrerer Dateien in eine einzelne ZIP-Datei, wodurch Speicherplatz gespart wird.

- **Gzip:**
 Gzip ist ein Dateikompressionsprogramm, das oft in Verbindung mit dem Tar-Programm verwendet wird. Es ist auf UNIX-basierten Systemen verbreitet und eignet sich gut für die Kompression von einzelnen Dateien.

- **RAR:** 
Das RAR-Format wird häufig für die Kompression von Dateien und Ordnern verwendet. Es ermöglicht die Erstellung von archivierten Dateien und bietet verschiedene Kompressionsstufen.

 **Weshalb ist Datenkompression insbesondere bei einem Image-Backup wichtig?**

 - **Übertragungsgeschwindigkeit:**
  Durch die Kompression von Image-Backups werden die Datenmengen reduziert, die übertragen werden müssen. Dies führt zu schnelleren Übertragungsgeschwindigkeiten, insbesondere bei der Sicherung großer Datensätze.

- **Zeit sparen:**
Da weniger Daten übertragen werden müssen, wird die Backup-Zeit erheblich reduziert. Dies ist besonders wichtig, wenn regelmäßige Backups durchgeführt werden und Effizienz von Bedeutung ist.

- **Tiefere Netzwerkbelastung:** 
Komprimierte Daten verursachen eine geringere Belastung des Netzwerks während der Übertragung. Dies ist insbesondere bei Remote-Backups über das Internet von Bedeutung, um die Bandbreite effizient zu nutzen und andere Netzwerkanwendungen nicht zu beeinträchtigen.