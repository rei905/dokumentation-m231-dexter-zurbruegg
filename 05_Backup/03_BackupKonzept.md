# Backupkonzept

## 1. Welche Funktionen / Services können beim Verlust des persönlichen Smartphones nicht mehr verwendet werden?

- **Telefonanrufe und SMS:**
  Bei einem Verlust des Smartphones ist die direkte Nutzung von Telefonanrufen und SMS nicht mehr möglich. Die Kommunikation über diese Dienste wird vorübergehend beeinträchtigt.

- **Kontakte:**
  Die Kontakte, die auf dem verlorenen Smartphone gespeichert waren, sind nicht mehr zugänglich. Neue Kontakte können nicht hinzugefügt oder bearbeitet werden.

- **Social Media:**
  Apps für soziale Medien, wie Facebook, Instagram oder Twitter, können nicht mehr über das verlorene Smartphone genutzt werden. Das Teilen von Updates und die Interaktion mit anderen Benutzern sind vorübergehend eingeschränkt.

- **Fotos & Dateien:**
  Zugriff auf gespeicherte Fotos und Dateien, die auf dem verlorenen Smartphone gespeichert waren, ist nicht mehr möglich. Neue Fotos können nicht aufgenommen und gespeichert werden.

## 2. Welche Daten sind beim Verlust betroffen?

- **Kontaktinformationen:**
  Persönliche Kontaktinformationen, einschliesslich Telefonnummern, E-Mail-Adressen und eventuell auch Anschriften, sind betroffen.

- **Fotos & Dateien:**
  Jegliche gespeicherten Fotos, Videos und Dateien auf dem verlorenen Smartphone sind gefährdet.

- **Dokumente:**
  Möglicherweise sind auch wichtige Dokumente, wie PDFs oder Textdateien, betroffen.

- **E-Mails:**
  E-Mails, die auf dem Smartphone empfangen und gespeichert wurden, sind ohne Zugriff.

- **Social Media:**
  Aktivitäten, Beiträge und persönliche Informationen in sozialen Medien sind gefährdet.

## 3. Wo liegt das Backup der Daten?

- **Cloud (iCloud oder Google Drive):**
  Ein Großteil der Nutzer sichert ihre Daten in der Cloud, entweder über iCloud (für Apple-Geräte) oder Google Drive (für Android-Geräte). Hier werden automatisch Backups von Kontakten, Fotos, Dateien und Einstellungen erstellt.

- **Lokale Backups:**
  Manche Nutzer bevorzugen auch lokale Backups, die auf einem Computer oder einer externen Festplatte gespeichert werden. Diese müssen manuell erstellt werden und erfordern regelmäßige Pflege.

## 4. Wie können die Daten wiederhergestellt werden?

- **Durch die Cloud:**
  Nach dem Verlust des Smartphones können die gesicherten Daten mithilfe der Cloud-Dienste wie iCloud oder Google Drive auf einem neuen Gerät wiederhergestellt werden. Dies erfordert die Anmeldung mit dem entsprechenden Konto.

- **Lokale Backups:**
  Bei Verwendung von lokalen Backups können die Daten mithilfe der Backup-Dateien auf dem Computer oder der externen Festplatte zurückgespielt werden. Dies erfordert ebenfalls die Verbindung mit einem neuen Gerät und die Nutzung der Backup-Optionen.
