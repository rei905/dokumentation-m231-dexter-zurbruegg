# Hardwareausfall Laptop und Handy (Probesterben)

1: *welche Daten sind nicht verfügbar?*
- Dokumente
- Chats
- Passwörter

2: *Wie komme ich an welche Daten wieder ran?*
- Backup-Keys/Cloud

3: *Komme ich ohne Handy auf einem neuen Rechner wieder ran?*
- Ja, aber begrenzt (nicht alle Daten)

4: *Wo habe ich meine Passwörter?*
- Passwort-Manager (KeePass & SecureSafe)

5: *Welchen Zugriff hat ein "Finder" meiner Geräte?*
- Keine

6: *welchen Schaden kann der "Finder" mit den Daten anrichten?*
- Keine

7: *Kann ich die Daten fernlöschen?*
- Die Daten in der Cloud schon. Die lokalen Daten nicht.

8: *Kann ich die Geräte orten?*
- Handy: Ja.
- Laptop: Eher nicht.

9: *Gegen welche Gefahren sind meine Backups gesichert?*
- Keine.

10: *Wie beurteilen Sie Ihren Zeitaufwand?*
- 1-2 Stunden

11: *Was ist meine Konsequenz aus dem Experiment?*

- Ich kann nicht auf sehr viele Daten wieder zugreifen.

12: *Wie beurteilen Sie diese Übung?*
- Ich müsste mehr auf das Backup meiner Daten achten.

13: *Sind die Geräte und der Schaden versichert?*
- Nein.

14: *Muss in einem ersten Moment etwas gesperrt werden?*
- ApplePay, Postfinance-Karte


