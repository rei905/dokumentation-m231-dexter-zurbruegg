# Vor- und Nachteile Cloud

## Vorteile Cloud:

- **Immer übers Internet erreichbar:**
  Der Zugriff auf Daten in der Cloud ist von überall mit Internetverbindung möglich. Dies ermöglicht eine flexible und ortsunabhängige Nutzung.

- **Keine Hardware:**
  Nutzer benötigen keine physische Hardware für die Datenspeicherung. Dies reduziert Kosten und erleichtert die Skalierbarkeit.

- **Über alle gängigen Geräte-Typen erreichbar:**
  Cloud-Services sind plattformübergreifend, sodass Benutzer von verschiedenen Gerätetypen wie Computern, Tablets oder Smartphones auf ihre Daten zugreifen können.

- **Weitere Funktionen: Dateien-Teilen, usw.:**
  Die Cloud bietet zusätzliche Funktionen wie die einfache Möglichkeit, Dateien mit anderen zu teilen, kollaborativ an Dokumenten zu arbeiten und automatische Synchronisierung zwischen verschiedenen Geräten.

## Nachteile Cloud:

- **Ab einer gewissen Menge Daten: Regelmässige Kosten:**
  Bei großen Datenmengen können regelmäßige Kosten für den Speicherplatz in der Cloud anfallen. Dies sollte bei der langfristigen Nutzung berücksichtigt werden.

- **Cloud-Server ist vielleicht in einem anderen Land:**
  Der physische Standort des Cloud-Servers kann in einem anderen Land sein, was möglicherweise rechtliche und datenschutzrechtliche Fragen aufwirft.

- **Datenschutzbedenken: Zugriff von "Unbekannt"? Erfahre ich davon?:**
  Es besteht die Sorge um Datenschutz, insbesondere wenn Daten von unbefugten Dritten zugegriffen werden könnten. Es ist wichtig, die Sicherheitsmaßnahmen des Cloud-Anbieters zu überprüfen.

- **Kein physischer Zugang zu den Daten: Abhängig von externer Firma:**
  Nutzer haben keinen physischen Zugang zu den in der Cloud gespeicherten Daten, was die Abhängigkeit von der Zuverlässigkeit des Cloud-Anbieters erhöht.

- **Abhängig vom Internet(-Zugang):**
  Die Nutzung der Cloud erfordert eine ständige Internetverbindung. Ohne Internetzugang können Daten möglicherweise nicht abgerufen werden.

- **Was, wenn der Cloud-Anbieter ein Problem hat (Mein Zugang / Daten-Leaks / Daten gehen verloren):**
  Es besteht das Risiko, dass Probleme beim Cloud-Anbieter auftreten, wie der Verlust des Zugangs, Datenlecks oder der Verlust von Daten. Benutzer sind von den Sicherheitsmaßnahmen und der Zuverlässigkeit des Anbieters abhängig.
