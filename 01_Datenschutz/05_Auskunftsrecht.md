# Auskunftsrecht

Im DSG steht, dass jede Person vom Verantwortlichen nach Auskunft über seine persönlichen Daten verlangen darf. Sie können auch nach bedarf der betroffenen Person auch gelöscht werden.

Angenommen Sie wollen wissen, welche Personendaten Ihr Mobilfunkanbieter über Sie gespeichert hat. Wie müssen Sie vorgehen?

1. **Datenschutzerklärung und -richtlinien überprüfen:**
Ich beginne damit, die Datenschutzerklärung und -richtlinien meines Mobilfunkanbieters zu lesen. Dort sollten Informationen darüber stehen, welche Daten gesammelt werden und wie sie verwendet werden.
Es ist wichtig, die geltenden Datenschutzgesetze und -bestimmungen zu prüfen, um sicherzustellen, dass mein Auskunftsersuchen den rechtlichen Anforderungen entspricht.

2. **Kontakt mit dem Kundenservice:**
 Als nächstes rufe ich den Kundenservice meines Mobilfunkanbieters an oder sende eine E-Mail. Ich bitte höflich darum, mir mitzuteilen, welche personenbezogenen Daten über mich gespeichert sind.

3. **Datenschutzbehörde kontaktieren:** 
Sollte der Mobilfunkanbieter nicht angemessen auf meine Anfrage reagieren oder falls ich Bedenken bezüglich meiner persönlichen Daten habe, kann ich die Datenschutzbehörde in der Schweiz kontaktieren.

4. **Formelles Auskunftsersuchen:**
 Falls die Informationen aus der Datenschutzerklärung und der Kontaktaufnahme mit dem Kundenservice nicht ausreichen oder keine zufriedenstellende Antwort bringen, kann ich ein formelles Auskunftsersuchen stellen. Dies kann in schriftlicher Form per Brief oder E-Mail erfolgen und sollte klar formuliert sein. In meinem Schreiben kann ich den Mobilfunkanbieter auffordern, mir detaillierte Informationen über die gespeicherten personenbezogenen Daten, den Verwendungszweck, die Empfänger dieser Daten und die Dauer der Speicherung zur Verfügung zu stellen.