# Datenschutz VS. Datensicherheit

**Datensicherheit:**

Technische Methoden zur Gewährleistung der Schutzziele Vertraulichkeit, Verfügbarkeit und Integrität sind von entscheidender Bedeutung. Dazu gehören Verschlüsselungstechnologien, Firewalls, regelmäßige Sicherheitsaudits und -überprüfungen. Der Zugriff auf sensible Daten sollte auf autorisierte Personen beschränkt sein, und es sollten Mechanismen vorhanden sein, um unbefugten Zugriff zu verhindern. Schulungen für Mitarbeiter sind ein wesentlicher Bestandteil, um sicherzustellen, dass sie sich bewusst sind, wie sie sicher mit Daten umgehen können. Die Verfügbarkeit von Daten wird durch redundante Systeme und Notfallpläne gewährleistet, um Ausfälle zu minimieren und einen kontinuierlichen Zugriff zu ermöglichen.

**Datenschutz:**

Datenschutz umfasst Gesetze und Vorschriften, die den Umgang und Schutz persönlicher Daten regeln. Gesetze wie das Datenschutzgesetz (DSG) legen fest, wie Organisationen personenbezogene Daten sammeln, speichern, verarbeiten und weitergeben dürfen. Die Einhaltung dieser Gesetze ist entscheidend, um die Privatsphäre und die Rechte der betroffenen Personen zu schützen. Datenschutzbestimmungen sehen vor, dass Unternehmen transparent darüber informieren müssen, welche Daten sie sammeln, zu welchem Zweck sie verwendet und weitergegeben werden und wie lange sie gespeichert bleiben. Zudem müssen Unternehmen sicherstellen, dass die Daten angemessen geschützt sind und nur für den vorgesehenen Zweck verwendet werden.