# Checkliste datenschutz.ch

# PC - Sicherheit erhöhen - in fünf Schritten

1. Persönliche Informationen schützen

- Ich gebe bereits nicht sehr viele persönliche Informationen preis und surfe auch normalerweise ziemlich anonym im Internet.

2. Angriffe abwehren

- Ich führe normalerweise immer Sicherheitsupdates durch und benutze auch einen Antivirus/Firewall.

3. Zugriffe Unberechtigter verhindern

- Manchmal wiederhole ich ein paar Passwörter aber normalerweise benutze für alle Webseiten/Applikation verschiedene Passwörter. Nach jedem weggehen von meinem Gerät sperr ich den Bildschirm.

4. Verschlüsseln Sie sensitive Inhalte

- Ich benutze bereits einen verschlüsselten Datenträger (BitLocker).

5. Sicher Sie Informationen und löschen Sie Daten vollständig

- Ich führe momentan nicht sehr viele Datensicherungen durch. In diesem Aspekt muss ich mich noch verbessern.