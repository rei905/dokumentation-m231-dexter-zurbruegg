# Checkliste datenschutz.ch

# Daten im HomeOffice schützen

1. Installieren Sie alle Updates

- Ja, ich habe alle Updates installiert

2. Schalten Sie die Firewall und den Virenschutz ein

- Ja, ich habe die Firewall an und benutze den Windows Defender Antivirus.

3. Verwenden Sie starke Passwörter

- Ich benutze mehrheitlich sichere Passwörter, die ich mit einem Password-Manager verwalte. Manchmal benutze ich jedoch manchmal dasselbe Passwort für einige Services.

4. Geben Sie Ihre Passwörter nie weiter

- Ja, ich behalte bereits all meine Passwörter privat.

5. Schützen Sie Personendaten und geschäftliche Informationen

- Ich übe bereits diese Sicherheitsmassnahmen aus.

6. Setzen Sie E-Mail sicher ein

- Ich trenne bereits private und geschäftliche E-Mails und verwende zudem verschiedene Email-Tools.

7. Schützen Sie sich vor Phishing und anderen Bedrohungen

- Ich öffne bereits nur wichtige Emails- von denen ich weiss, dass sie sicher sind. 

8. Kommunizieren Sie sicher

- Ich benutze nicht immer eine VPN aber meistens schon. Für vertrauliche Daten benutze ich meistens eine Email-Applikation.

9. Melden Sie Datenverlust sofort

- Ich melde mich normalerweise sofort beim IT-Support.

10. Schalten Sie das Cloud-Backup aus

- Momentan benutze ich weiterhin die Cloud von Microsoft und Apple, aber ich versuche auf andere Cloud-Lösungen zu wechseln.