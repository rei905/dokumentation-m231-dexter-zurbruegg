# Cookies

1.  **Je nach Webseite, welche Sie aufrufen, erhalten Sie unterschiedliche Optionen zur Auswahl.**

    **Welche Möglichkeiten finden Sie?**

    - Ablehnen
    - Akzeptieren
    - Nur notwendige Cookies akzeptieren
    - bestimmte Cookies akzeptieren (Auswahl)

2.  **Warum werden seit einiger Zeit solche Banner angezeigt? Seit wann?**

    **Ist das nur auf Schweizer Websites der Fall?**

    - Diese Cookie-Banner sind bereits in den letzten Jahren auf viele Webseiten durch viele Abkommen, insbesondere dem DSVGO-Abkommen der europäischen Union, aufgetreten. Die DSGVO schreibt vor, dass Websites transparent über die Verwendung von Cookies informieren müssen und den Nutzern die Möglichkeit geben müssen, der Verwendung von bestimmten Cookies zuzustimmen oder sie abzulehnen.

3.  **Was sind Cookies?**
    **Welche Arten von Cookies werden unterschieden?**
    **Wo kann man diese im Browser anschauen?**
    **Braucht es Cookies?**
    **Welche Ziele haben die Besitzer der Webseiten?**
    **Was möchten Sie erreichen?**
    **Warum?**

    **Was sind Cookies?**

    - Cookies sind kleine Textdateien, die von einer Website auf Ihrem Computer oder Mobilgerät gespeichert werden, wenn Sie die Website besuchen. Sie dienen dazu, Informationen über Ihre Aktivitäten auf der Website zu speichern und abzurufen.

    **Welche Arten von Cookies werden unterschieden?**

    - Session-Cookies: Werden temporär während Ihrer Browsersitzung gespeichert und gelöscht, wenn Sie den Browser schließen.

    - Permanente Cookies: Werden längerfristig auf Ihrem Gerät gespeichert, auch nachdem Sie den Browser geschlossen haben. Sie können zum Beispiel Ihre Anmeldedaten oder Präferenzen speichern.

    - Erstanbieter-Cookies: Werden von der besuchten Website selbst erstellt und können für verschiedene Zwecke verwendet werden.

    - Drittanbieter-Cookies: Werden von anderen Domains als der besuchten Website erstellt. Häufig werden sie für Werbung und Analytik verwendet.

    **Wo kann man diese im Browser anschauen?**

    - Google Chrome: Einstellungen > Erweitert > Datenschutz und Sicherheit > Website-Einstellungen > Cookies und Website-Daten.

    **Braucht es Cookies?**

    - Cookies sind in vielen Fällen notwendig, um bestimmte Funktionen auf Websites zu ermöglichen. Zum Beispiel erleichtern sie das Speichern von Anmeldedaten, das Verfolgen von Warenkorbaktivitäten beim Online-Shopping und das Anpassen von Inhalten basierend auf den Präferenzen.

    **Welche Ziele haben die Besitzer der Webseiten?**

    - Verbesserte Benutzererfahrung: Cookies ermöglichen personalisierte Inhalte und Funktionen, die auf Ihre Präferenzen zugeschnitten sind.

    - Analytik: Cookies werden verwendet, um das Nutzerverhalten zu analysieren und wertvolle Einblicke in die Website-Nutzung zu gewinnen.

    - Werbung: Drittanbieter-Cookies werden häufig für gezielte Werbung verwendet, um Anzeigen basierend auf Ihrem Online-Verhalten zu personalisieren.

    - Funktionalität: Cookies unterstützen die Funktionalität von Websites, indem sie Informationen speichern, um eine nahtlose Navigation zu ermöglichen.

    **Warum?**

    - Die Verwendung von Cookies ermöglicht es Website-Besitzern, ihren Nutzern eine effizientere und personalisierte Erfahrung zu bieten. Darüber hinaus kann Personalisierte Werbung, basierend auf dem Nutzerverhalten, auch für die Monetarisierung von Websites wichtig sein.

4.  **Was haben diese Banner mit “Datenschutz” zu tun?**
    **Worum geht es beim Datenschutz?**
    **Was sind “persönliche Daten”?**
    **Welche Reaktion auf die Banner würden Sie BenutzerInnen empfehlen?**
    **Sollen bspw. immer alle akzeptiert werden?$**

    **Was haben diese Banner mit "Datenschutz" zu tun?**

    - Die Cookie-Banner sind eine Reaktion auf Datenschutzgesetze wie die DSGVO, die Websites dazu verpflichten, Benutzer darüber zu informieren, welche Daten gesammelt werden und wie sie verwendet werden. Die Banner bitten Benutzer um Zustimmung zur Verwendung von Cookies, da Cookies oft dazu verwendet werden, persönliche Daten zu sammeln und zu verarbeiten. Indem Benutzer die Möglichkeit haben, Cookies zu akzeptieren oder abzulehnen, wird ihre Privatsphäre und Kontrolle über ihre Daten respektiert.

    **Worum geht es beim Datenschutz?**

    - Datenschutz bezieht sich auf die Kontrolle und den Schutz der persönlichen Informationen einer Person. Der Datenschutz zielt darauf ab, sicherzustellen, dass die persönlichen Daten von Einzelpersonen respektiert, sicher gespeichert und nicht ohne Zustimmung verwendet werden. Datenschutzbestimmungen und Gesetze, wie die Datenschutz-Grundverordnung (DSGVO) in der Europäischen Union, legen Regeln fest, die Organisationen befolgen müssen, um die Privatsphäre der Nutzer zu schützen.

    **Was sind "perösonliche Daten"?**

    - Persönliche Daten umfassen Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person beziehen. Dazu gehören Namen, Adressen, E-Mail-Adressen, Telefonnummern, IP-Adressen, Standortdaten, Online-Identifikatoren und mehr.

    **Welche Reaktion auf die Banner würden Sie BenutzerInnen empfehlen?**

    - Informieren Sie sich: Lesen Sie die Cookie-Hinweise und Datenschutzrichtlinien der Websites, um zu verstehen, welche Arten von Cookies verwendet werden und wie die Daten verarbeitet werden.

    - Individualisieren Sie Ihre Einstellungen: Nutzen Sie die Optionen im Cookie-Banner, um Ihre Präferenzen anzupassen. Sie können in der Regel auswählen, welche Arten von Cookies Sie akzeptieren möchten.

    - Kritisch sein: Überlegen Sie genau, ob Sie allen Cookies zustimmen möchten. Ein differenzierter Ansatz ermöglicht es Ihnen, nur die Cookies zu akzeptieren, die für die gewünschte Funktionalität der Website notwendig sind.

    Sollen bspw. immer alle akzeptiert werden?

    - Die Entscheidung, alle Cookies zu akzeptieren oder nicht, hängt von Ihren persönlichen Vorlieben, Datenschutzüberlegungen und den Funktionen ab, die Sie auf einer bestimmten Website nutzen möchten.

    **Gründe, alle Cookies zu akzeptieren:**

    - Volle Funktionalität: Einige Websites benötigen bestimmte Cookies, um ordnungsgemäß zu funktionieren. Wenn man alle Cookies ablehnt, könnte dies die Benutzererfahrung beeinträchtigen, indem bestimmte Funktionen nicht verfügbar sind.

    - Anmeldung und Präferenzen: Cookies können dazu verwendet werden, sich an die Anmeldedaten und Präferenzen zu erinnern. Das Akzeptieren von Cookies kann dazu beitragen, dass man bei wiederholten Besuchen einer Website nicht jedes Mal erneut Anmeldedaten eingeben muss.

    **Gründe, sie abzulehnen:**

    - Datenschutz: Das Akzeptieren aller Cookies kann bedeuten, dass man einer umfassenden Datensammlung zustimmen muss, insbesondere wenn es um Drittanbieter-Cookies für Werbezwecke geht. Dies könnte die Privatsphäre beeinträchtigen.

    - Gezielte Werbung: Wenn man personalisierte Werbung nicht wünscht, kann man bestimmte Arten von Cookies, die für gezielte Anzeigen verwendet werden, ablehnen.

5.  **Was passiert, wenn jemand eine Option akzeptiert hat? Kann die Person Ihre Meinung danach wieder ändern? Wenn ja: Wie?**

    - Ja, in der Regel können Benutzer ihre Zustimmung zu Cookies ändern, nachdem sie eine Option akzeptiert haben. Die meisten Websites, die Cookie-Banner verwenden, bieten den Benutzern die Möglichkeit, ihre Cookie-Einstellungen zu verwalten. Hier sind einige Schritte, die Sie normalerweise befolgen können:

    - Website-Einstellungen: Viele Websites haben einen Link zu den Datenschutzeinstellungen oder den Cookie-Einstellungen, der oft am Ende der Website oder im Bereich "Datenschutz" zu finden ist.

    - Cookie-Verwaltungsbereich: In den Datenschutzeinstellungen finden Sie oft einen Abschnitt, der sich auf Cookies bezieht. Hier können Sie Ihre Cookie-Präferenzen ändern.

6.  **Welche gesetzlichen Vorgaben in Sachen Datenschutz gibt es für die Firmen? Was heisst das für Sie, als Person, die in der Informatik arbeitet?**

    Für Firmen:

    - Einwilligung: Unternehmen müssen die ausdrückliche Zustimmung der Benutzer einholen, bevor sie personenbezogene Daten verarbeiten können.

    - Rechte der Betroffenen: Die DSGVO gewährt den betroffenen Personen erweiterte Rechte, darunter das Recht auf Auskunft, das Recht auf Berichtigung und das Recht auf Löschung ihrer Daten.

    - Datenschutz durch Technikgestaltung und datenschutzfreundliche Voreinstellungen (Privacy by Design and by Default): Unternehmen sind verpflichtet, Datenschutzprinzipien bereits bei der Entwicklung von Produkten und Dienstleistungen zu berücksichtigen.

    Als Person, die in der Informatik arbeitet, bedeutet die DSGVO, dass Sie bei der Entwicklung von Software, der Implementierung von Systemen und der Verarbeitung von Daten besondere Sorgfalt auf den Schutz personenbezogener Informationen legen müssen. Dies kann Folgendes umfassen:

    - Datenschutz durch Design: Integrieren Sie von Anfang an Datenschutzprinzipien in die Entwicklung von Software und Systemen.

    - Datenschutz-Folgenabschätzung: Führen Sie gegebenenfalls eine Datenschutz-Folgenabschätzung durch, um die Auswirkungen auf die Privatsphäre zu bewerten.

    - Sicherheitsmaßnahmen: Implementieren Sie angemessene Sicherheitsmaßnahmen, um die Vertraulichkeit, Integrität und Verfügbarkeit von personenbezogenen Daten sicherzustellen.

    - Einwilligung und Transparenz: Stellen Sie sicher, dass Sie klare Einwilligungen für die Verarbeitung personenbezogener Daten erhalten und transparent über die Datenverarbeitungspraktiken informieren.

7.  **Einige Firmen werden mit dem Wort “Datenkraken” assoziiert. Nennen Sie Beispiele und Hintergründe!**

    Der Begriff "Datenkraken" wird oft verwendet, um auf Unternehmen hinzuweisen, die große Mengen an persönlichen Daten sammeln, speichern und nutzen, oft für kommerzielle Zwecke. Hier ist ein Beispiel von Facebook, das in der Vergangenheit als "Datenkraken" bezeichnet wurden:

    - Facebook: Facebook ist eines der größten sozialen Netzwerke der Welt und hat Zugang zu umfangreichen Mengen an persönlichen Informationen seiner Nutzer. Die Plattform hat in der Vergangenheit Kritik dafür erhalten, wie sie Daten für gezielte Werbung und andere Zwecke verwendet hat. Der Cambridge-Analytica-Skandal ist ein Beispiel, bei dem Daten von Millionen Facebook-Nutzern ohne ihre Zustimmung gesammelt wurden.
